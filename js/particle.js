particlesJS(
    'myIdName', {
        'particles': {
            'number': {
                'value': 16 // set dots numbers ←
            },
            'color': {
                // set colors and number of colors ↓
                'value': ['#FFB74D', '#FF5252', '#7C4DFF','#00B8D4']
            },
            'shape': {
                'type': 'circle'
            },
            'opacity': {
                'value': 1,
                'random': false,
                'anim': {
                    'enable': false
                }
            },
            'size': {
                'value': 4, // set dots size ←
                'random': true,
                'anim': {
                    'enable': false,
                }
            },
            'line_linked': {
                'enable': false
            },
            'move': {
                'enable': true,
                'speed': 4, // set speed ←
                'direction': 'none',
                'random': true,
                'straight': false,
                'out_mode': 'out'
            }
        },
        'interactivity': {
            'detect_on': 'canvas',
            'events': {
                'onhover': {
                    'enable': false
                },
                'onclick': {
                    'enable': false
                },
                'resize': true
            }
        },
        'retina_detect': true
    });