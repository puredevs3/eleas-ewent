const button = document.querySelector('.toggle-button');
const hiddenItems = document.querySelectorAll('.hidden-item');
let isHidden = true;
button.addEventListener('click', () => {
    button.textContent = isHidden
        ? 'Hide items'
        : 'Load more';

    isHidden = !isHidden;
    hiddenItems.forEach(item => item.classList.toggle('hidden'));
});


var selector = '.list li';

$(selector).on('click', function(){
    $(selector).removeClass('active');
    $(this).addClass('active');
});


// particle js
particlesJS(
    'myIdName', {
        'particles': {
            'number': {
                'value': 16 // set dots numbers ←
            },
            'color': {
                // set colors and number of colors ↓
                'value': ['#FFB74D', '#FF5252', '#7C4DFF','#00B8D4']
            },
            'shape': {
                'type': 'circle'
            },
            'opacity': {
                'value': 1,
                'random': false,
                'anim': {
                    'enable': false
                }
            },
            'size': {
                'value': 4, // set dots size ←
                'random': true,
                'anim': {
                    'enable': false,
                }
            },
            'line_linked': {
                'enable': false
            },
            'move': {
                'enable': true,
                'speed': 4, // set speed ←
                'direction': 'none',
                'random': true,
                'straight': false,
                'out_mode': 'out'
            }
        },
        'interactivity': {
            'detect_on': 'canvas',
            'events': {
                'onhover': {
                    'enable': false
                },
                'onclick': {
                    'enable': false
                },
                'resize': true
            }
        },
        'retina_detect': true
    });

var a = 0;
$(window).scroll(function() {

    var oTop = $('#counter').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
        $('.counter-value').each(function() {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.text()
            }).animate({
                    countNum: countTo
                },

                {

                    duration: 2000,
                    easing: 'swing',
                    step: function() {
                        $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                        $this.text(this.countNum);
                        //alert('finished');
                    }

                });
        });
        a = 1;
    }

});

// Basic initialization is like this:
// $('.your-class').slick();

// I added some other properties to customize my slider
// Play around with the numbers and stuff to see
// how it works.
$('.slick-carousel').slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 2,
    autoplay: false,
    autoplaySpeed: 2000,
    arrows: true,
    nextArrow: '<div class="slick-custom-arrow-right"><img src="images/arrow-right.png" alt="" class="img-fluid d-block arrow-right-icon"></div>',
    prevArrow: '<div class="slick-custom-arrow-left"><img src="images/arrow-left.png" alt="" class="img-fluid d-block arrow-left-icon"></div>',

    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

$(document).ready(function() {



});